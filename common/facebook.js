var userId;
function statusChangeCallback(response) {
    if (response.status === 'connected') {
      testFacebookAPI();
    } else if (response.status === 'not_authorized') {
      FB.login(function(response) {
        retryStatusChangeCallback(response);
      }, {scope: 'public_profile, email'});

    } else {
      console.log("Error: Not connected, not logged in Facebook");
    }
}

function retryStatusChangeCallback(response) {
    console.log('Retry callback');
    console.log(response);
    if (response.status === 'connected') {
      testFacebookAPI();
    } else if (response.status === 'not_authorized') {
      console.log('Not authorized yet.');
    } else {
      console.log("Error: Not connected or not logged in Facebook.");
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

function testFacebookAPI() {
    FB.api('/me', {fields: 'name, email'}, function(response) {
      console.log('Successful login for: ' + response.id);
      var xhttp = new XMLHttpRequest();
      xhttp.open("POST", "https://saracarrion.com/trivial/backend/users.php", true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send("email=" + response.email + "&name=" + response.name + "&id=" + response.id);
      userId = response.id;
      console.log(userId);
      $("#custom").val(userId);
      console.log(userId);
    });
}

$(document).ready(function() {
    FB.init({
      appId      : '321116627986140',
      status     : true,
      xfbml      : true,
      version    : 'v2.2'
    });

    checkLoginState();

    /*$("#buy_now").click(function(){
        FB.ui({
              method: 'pay',
              action: 'purchaseitem',
              product: 'www.saracarrion.com/trivial/payment/coins.html',
              quantity: 10,
              quantity_min: 1,
              quantity_max: 100,
            },
            paymentDoneCallback
        );
    });*/
});

function paymentDoneCallback(response){
    console.log("Payment called");
    console.log(response);
}
