<?php
  
    if($_POST){
        $response = validate_call($_POST);
        if (!empty($response)){
            echo json_encode($response);
            exit;
        }
        $response = get_result($_POST['project']);
    }else{
        $response['status'] = false;
        $response['error'] = "Critical error";
    }
    $response['project'] = $_POST['project'];
    echo json_encode($response);

function validate_call($call){
    check_id($call['id']);
    check_token($call['token']);
    check_degrees($call['degrees']);
    check_project($call['project']);
}

function check_id($id){
    if($id != '39403393'){
        echo json_encode( array('status' => 'false', 'error1' => "true") );
        exit;
    }
}

function check_token($token){
    if($token != 'Z9df0a7dgS1ej1Em37grfa24o8RL'){
        echo json_encode( array('status' => 'false', 'error2' => "True") );
        exit;
    }
}

function check_degrees($degrees){
    if($degrees > 360 OR $degress < 0){
        echo json_encode( array('status' => 'false', 'error3' => "True") );
        exit;
    }
}

function check_project($project_id){
    if($project_id == 1001 || $project_id == 2001){
       return;
    }
    echo json_encode( array('status' => 'false', 'error4' => "True") );
    exit;
}

function calculate_prize($degrees){
    $slices = 8;
    $slicePrizes = ["10", "50", "500", "0", "200", "100", "150", "0"];
    $prize = ($slices - 1) - floor($degrees / (360 / $slices));
    return $slicePrizes[$prize];
}

function get_result($project_id){    
    switch($project_id){
        case 1001:
            $response = get_wheel_response($project_id);
            break;
        case 2001:
            $response = get_slot_response($project_id);
            break;
        default:
    }
    return $response;   
}

function get_wheel_response($project_id){
    $rand = rand(0, 360);
    $response = array('status' => 'true');
    $response['degrees'] = $rand;
    $response['id'] = $_POST['id'];
    $response['token'] = $_POST['id'];
    $response['bet'] = calculate_prize($rand);
    return $response;
}

function get_slot_response($project_id){
    $rand1 = rand(0, 8);
    $rand2 = rand(0, 8);
    $rand3 = rand(0, 8);
    $rand4 = rand(0, 8);
    $response['reel1'] = $rand1 % 2 == 0 ? 'Life Mushroom' : 'Goomba';
    $response['reel2'] = $rand2 % 2 == 0 ? 'Life Mushroom' : 'Fire Flower';
    $response['reel3'] = $rand3 % 2 == 0 ? 'Life Mushroom' : 'Mario Hat';
    $response['reel4'] = $rand4 % 2 == 0 ? 'Life Mushroom' : 'Question Coin';
    $response['reel5'] = 'Goomba';
    $response['status'] = true;
    if($response['reel1'] == $response['reel2']  && $response['reel2'] == $response['reel3'] && $response['reel1'] == $response['reel4']   ){
        $response['prize'] = 500;
    }else if($response['reel1'] == $response['reel2'] ){
        $response['prize'] = 100;
    }else{
        $response['prize'] = 0;
    }
    return $response;
}

?>
