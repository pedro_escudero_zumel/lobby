# Components Lobby Repository #

Brief explanation about this repository structure and components:

backend: Fake backend files in PHP used during client creation. We will discard them when we have the final backend.

common: Files that are common for all games and the lobby.

footer: We can ignore it from now (footer is the same in all games and in the lobby, so we need to consider a refactor later to avoid to have a repat code in all pleaces).

header: Same that above, but for the header.

lobby: Common files for the lobby (css, images, sounds, etc)

payment: Needed files for handling payments in Facebook.

index.html: Lobby main page, with the links to the games.

## Aplication structure ##

I add this info here as a reference.

Client structure in the server:
```
/index.html

	/common -> from lobby repository
	
	/lobby  -> from lobby repository
	
	/payment -> from lobby repository
	
	/slots -> from slots repository
	
	/wheel -> from golden-wheel repository
```

